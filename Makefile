# Top-Level Project Makefile
# (Compatible with GNU make)
#
# Copyright (c) 2017-2022 Paul Mullen (pm@nellump.net)
# SPDX-License-Identifier: MIT


SHELL = /bin/bash
MAKEFLAGS = --warn-undefined-variables

include project.mk
include sequences.mk



# Configuration
#

SRC_DIR = $(CURDIR)/src
META_DIR = $(CURDIR)/meta
BUILD_DIR = $(CURDIR)/build
TEST_DIR = $(CURDIR)/test
ARCHIVE_DIR = $(CURDIR)
DEBIAN_DIR = $(CURDIR)/debian

# For the children!
export CURDIR0 = $(CURDIR)
export SRC_DIR META_DIR

export SUBST_VARS=$(META_DIR)/substvars.sh

# install/uninstall
INSTALL_CFG = $(META_DIR)/install.txt
INSTALL_CFG_FIELDS  =  source dest mode owner group
PREFIX ?= /usr/local
# Set by `debuild`:
DESTDIR ?= $(PREFIX)



# Targets
#

.PHONY: all build test \
		install uninstall \
		issues changelog \
		release erchive dpkg \
		clean distclean


all: build


build:
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)
	mkdir -p $(BUILD_DIR)
	$(call export_substvars) ; \
	cat $(INSTALL_CFG) | $(strip_comments) | $(substitute_vars) | $(call install_files,$(SRC_DIR),$(BUILD_DIR))


test: export TEST_PATH = $(TEST_PATH_DIRS)
test: clean build
	$(call echo_heading)
#	bats --no-tempdir-cleanup -r $(TEST_DIR)
	bats -r $(TEST_DIR)


install: test
	$(call echo_heading)
	[[ -d $(DESTDIR) ]]
	$(call export_substvars) ; \
	cat $(INSTALL_CFG) | $(strip_comments) | $(substitute_vars) | $(call install_files,$(SRC_DIR),$(DESTDIR))


uninstall:
	$(call echo_heading)
	$(call export_substvars) ; \
	cat $(INSTALL_CFG) | $(strip_comments) | $(substitute_vars) | while read $(INSTALL_CFG_FIELDS) ; do \
		rm -f $(DESTDIR)/$$dest ; \
	done


# TODO: Delete me?
issues:
	$(call echo_heading)
	@echo "NB: Merge from *issues* branch first!"
	cd $(META_DIR)  &&  cil summary --is-open | grep -v '===' > $(CURDIR)/ISSUES.txt


changelog: TAG = v.$(VERSION)
changelog:
	$(call echo_heading)
	@echo "NB: Close all issues being deployed with this release first!"
	cd $(META_DIR)  &&  cil summary --label $(TAG) | grep -v '===' > $(CURDIR)/CHANGES-$(TAG).txt


release: archive dpkg


archive:
	$(call echo_heading)
# No effect unless on "master" branch.
ifeq ($(GIT_BRANCH),master)
	mkdir -p $(ARCHIVE_DIR)
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).tgz
	git archive v.$(VERSION) --prefix $(NAME)_$(VERSION)/ --output $(ARCHIVE_DIR)/$(NAME)_$(VERSION).zip
else
	@echo "Switch to master branch before creating archives!"
endif


# debuild invokes the appropriate target dependencies.
dpkg:
	$(call echo_heading)
	debuild -us -uc -b


clean:
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)
	rm -fr $(BUILD_DIR)


distclean: clean
	$(call echo_heading)
	$(call exec_make_subdir,$(SRC_DIR),$@)
	git clean -xd --force
