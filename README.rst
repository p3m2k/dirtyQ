######
dirtyQ
######


Introduction
============

*dirtyQ* is a command line program that provides simple, file system
based queue and stack data structures.  Data structures can be
implemented as single files, in which each line is a data object, or
directories, in which each data object is stored in a separate file.


Why Not A FIFO?
---------------

Pipes and FIFOs work well for many purposes, and may be preferable for
some, but, they:

- Can't be directly inspected with common tools.
- Are volatile, and will not persist across system restarts.
- Don't support a stack structure.

On the other hand, there is no active management of reads and writes
(i.e., I/O blocking) with dirtyQ

Suffice it to say that dirtyQ is superficially similar to pipes and
FIFOs, but there are significant differences.


What's With The Name?
---------------------

“directory dequeue” -> “dir_dequeue” -> “dirty queue”  (So clever!)



Installation
============

dirtyQ is a Bash_ shell script.  Almost every Linux system includes
Bash.  Users of other Unix-like_ systems may need to install it before
using this program.  dirtyQ has no special dependencies beyond
bog-standard Unix command line utilities.

There are two easy ways to install dirtyQ:

- Packages_ are available for users of Debian and its derivatives
  (Ubuntu, etc.).  Users of Red Hat, Fedora and friends: stay tuned!

- Download an archive of the latest release and run ``make install``:

  .. code:: console

     # wget -O - https://www.nellump.net/downloads/dirtyq_latest.tgz | tar xz
     # cd dirtyq/
     # make install

bats_ is required to run the automated test suite (of interest only to
programmers).

.. _Bash: https://www.gnu.org/software/bash/
.. _Unix-like: http://www.linfo.org/unix-like.html
.. _packages : https://www.nellump.net/computers/free_software
.. _bats: https://github.com/bats-core/bats-core



Usage
=====

See the `manpage <src/man/dirtyq.1.raw.rst>`_.

dirtyQ pairs well with `Qrun <https://gitlab.com/p3m2k/Qrun>`_, a
command-line program that runs a command on each item in a dirtyQ-based
data structure. 



Future Plans
============

None at this time.



Copying
=======

Everything here is copyright © 2022 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
