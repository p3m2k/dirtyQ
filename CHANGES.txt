v.1.3.1 (2022-09-05)
===============================================================================
2f90af58   Update to latest Bash script template


v.1.3.0 (2022-06-22)
===============================================================================
69d814cb   Update to latest Bash script template
6e8c0555   Add subcommand to print length/size of data structure


v.1.2.0 (2022-06-10)
===============================================================================
99e4e387   "init" shouldn't fail on pre-existing dirtyQ structure


v.1.1.0 (2022-06-08)
===============================================================================
242f3ef5   Import Makefile "install" target fix
2740133f   Update README, manpage, etc. w/ references to Qrun
9884c9b9   Normalize project name in README, etc.
e5a2441a   Added exit value for attempts to dequeue/pop from empty data structure


v.1.0.0 (2022-05-31)
===============================================================================
Initial release.
