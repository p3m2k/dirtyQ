export swut="dirtyq"
dependencies="cat grep head tail"

expected_version="1.3.1"

expected_status_success=0
expected_status_error=1
expected_status_empty=2


expected_usage_short="$swut [-h] [-v] [-V] [-d] init|length|enqueue|dequeue|push|pop PATH DATA"
expected_usage_long="$expected_usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -d    Create directory-based data structure"

# Not quite exactly what an freshly-initialized dirtyQ file should
# contain.  Truncate the date so we have a bit of fudge factor.
expected_magic_string="## $swut v.${expected_version} :: $(date --utc --rfc-3339=date)"
