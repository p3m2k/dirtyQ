load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash

load helpers



# Test support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!

    # What are the chances these won't be installed?
    assert_dependencies $dependencies

    export stdopts="-V"

    # Use ./test/temp so these persist across test cases:
    export path_run_queue_f="${BATS_RUN_TMPDIR}/test_f"
    export path_run_queue_d="${BATS_RUN_TMPDIR}/test_d"
}



# Common options
#

@test "no opts, no args" {
    run exec_swut

    assert_status $expected_status_error
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_usage_short" ]
}


@test "invalid opt" {
    run exec_swut -z

    assert_status $expected_status_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Invalid option: z" ]
    [ "${lines[1]}" = "$expected_usage_short" ]
}


@test "help" {
    run exec_swut -h

    assert_status $expected_status_success
    assert_lines_qty 6  # bats ignores blank lines.
    [ "$output" = "$expected_usage_long" ]
}


@test "version" {
    run exec_swut -v

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$swut $expected_version" ]
}


@test "verbose option" {
    run exec_swut -V

    assert_status $expected_status_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "$expected_usage_short" ]
}



# Script-specific
#

@test "enqueue to non-existent node" {
    local ds_path="/ds_path/to/nowhere"

    run exec_swut $stdopts enqueue $ds_path "this shouldn't work"

    assert_status $expected_status_error
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path does not exist: '$ds_path'" ]
    [ "${lines[2]}" = "Not a dirtyQ data structure: '$ds_path'" ]
}


@test "enqueue to uninitialized file" {
    local ds_path="${BATS_TEST_TMPDIR}/not_a_dirtyq_file"
    touch "$ds_path"

    run exec_swut $stdopts enqueue $ds_path "this shouldn't work"

    assert_status $expected_status_error
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is not a dirtyQ data structure: '$ds_path'" ]
    [ "${lines[2]}" = "Not a dirtyQ data structure: '$ds_path'" ]
}


@test "enqueue to uninitialized directory" {
    local ds_path="${BATS_TEST_TMPDIR}/not_a_dirtyq_dir"
    mkdir "$ds_path"

    run exec_swut $stdopts enqueue $ds_path "this shouldn't work"

    assert_status $expected_status_error
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is not a dirtyQ data structure: '$ds_path'" ]
    [ "${lines[2]}" = "Not a dirtyQ data structure: '$ds_path'" ]
}


@test "file, initialize existing, dirtyQ structure" {
    local ds_path="$(make_temp_ds)"

    run exec_swut $stdopts init $ds_path

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$ds_path'" ]
    [ "${lines[2]}" = "There's already a dirtyQ data structure here: '$ds_path'" ]
}


@test "file, initialize existing, non-dirtyQ" {
    local ds_path="${BATS_TEST_TMPDIR}/someone_elses_file"
    touch "$ds_path"

    run exec_swut $stdopts init $ds_path

    assert_status $expected_status_error
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is not a dirtyQ data structure: '$ds_path'" ]
    [ "${lines[2]}" = "There's already something here: '$ds_path'" ]
}





@test "file, initialize" {
    run exec_swut $stdopts init $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Initializing data structure: '$path_run_queue_f'" ]
}


@test "file, enqueue #1" {
    local new_data="first enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "file, enqueue #2" {
    local new_data="second enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "file, dequeue #1" {
    run exec_swut $stdopts dequeue $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "first enqueue" ]
}


@test "file, enqueue #3, stdin via pipe" {
    local new_data="third enqueue"

    runner() {
        bash -c "echo \"$new_data\" | exec_swut $stdopts enqueue $path_run_queue_f"
    }
    run runner

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "file, enqueue #4, stdin via redirect" {
    local new_data="fourth enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_f < <(echo "$new_data")

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "file, dequeue #2" {
    run exec_swut $stdopts dequeue $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "second enqueue" ]
}

@test "file, dequeue #3" {
    run exec_swut $stdopts dequeue $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "third enqueue" ]
}

@test "file, dequeue #4" {
    run exec_swut $stdopts dequeue $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "fourth enqueue" ]
}

@test "file, dequeue #5 (empty queue)" {
    run exec_swut $stdopts dequeue $path_run_queue_f

    assert_status $expected_status_empty
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
}





@test "file, push #1" {
    local new_data="first push"

    run exec_swut $stdopts push $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "file, push #2" {
    local new_data="second push"

    run exec_swut $stdopts push $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "file, pop #1" {
    run exec_swut $stdopts pop $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "second push" ]
}


@test "file, push #3" {
    local new_data="third push"

    run exec_swut $stdopts push $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "file, push #4" {
    local new_data="fourth push"

    run exec_swut $stdopts push $path_run_queue_f "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "file, pop #2" {
    run exec_swut $stdopts pop $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "fourth push" ]
}


@test "file, pop #3" {
    run exec_swut $stdopts pop $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "third push" ]
}


@test "file, pop #4" {
    run exec_swut $stdopts pop $path_run_queue_f

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
    [ "${lines[2]}" = "first push" ]
}


@test "file, pop #5 (empty stack)" {
    run exec_swut $stdopts pop $path_run_queue_f

    assert_status $expected_status_empty
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ file: '$path_run_queue_f'" ]
}





@test "directory, initialize" {
    run exec_swut $stdopts -d init $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Initializing data structure: '$path_run_queue_d'" ]
}





@test "directory, enqueue #1" {
    local new_data="first enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "directory, enqueue #2" {
    local new_data="second enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "directory, dequeue #1" {
    run exec_swut $stdopts dequeue $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "first enqueue" ]
}


@test "directory, enqueue #3, stdin via pipe" {
    local new_data="third enqueue"

    run bash -c "echo \"$new_data\" | exec_swut $stdopts enqueue $path_run_queue_d"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "directory, enqueue #4, stdin via redirect" {
    local new_data="fourth enqueue"

    run exec_swut $stdopts enqueue $path_run_queue_d < <(echo "$new_data")

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Enqueued data: '$new_data'" ]
}


@test "directory, dequeue #2" {
    run exec_swut $stdopts dequeue $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "second enqueue" ]
}


@test "directory, dequeue #3" {
    run exec_swut $stdopts dequeue $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "third enqueue" ]
}


@test "directory, dequeue #4" {
    run exec_swut $stdopts dequeue $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "fourth enqueue" ]
}


@test "directory, dequeue #5 (empty queue)" {
    run exec_swut $stdopts dequeue $path_run_queue_d

    assert_status $expected_status_empty
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
}





@test "directory, push #1" {
    local new_data="first push"

    run exec_swut $stdopts push $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "directory, push #2" {
    local new_data="second push"

    run exec_swut $stdopts push $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "directory, pop #1" {
    run exec_swut $stdopts pop $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "second push" ]
}


@test "directory, push #3" {
    local new_data="third push"

    run exec_swut $stdopts push $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "directory, push #4" {
    local new_data="fourth push"

    run exec_swut $stdopts push $path_run_queue_d "$new_data"

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "Pushed data: '$new_data'" ]
}


@test "directory, pop #2" {
    run exec_swut $stdopts pop $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "fourth push" ]
}


@test "directory, pop #3" {
    run exec_swut $stdopts pop $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "third push" ]
}


@test "directory, pop #4" {
    run exec_swut $stdopts pop $path_run_queue_d

    assert_status $expected_status_success
    assert_lines_qty 3
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
    [ "${lines[2]}" = "first push" ]
}


@test "directory, pop #5 (empty stack)" {
    run exec_swut $stdopts pop $path_run_queue_d

    assert_status $expected_status_empty
    assert_lines_qty 2
    [ "${lines[0]}" = "Verbose mode requested." ]
    [ "${lines[1]}" = "Specified path is a dirtyQ directory: '$path_run_queue_d'" ]
}



# vim: syntax=bash tw=0
