source_swut() {
    if [[ -z "$TEST_PATH" ]] ; then
        echo "# ${FUNCNAME[0]}: \$TEST_PATH unset!" >&3
        return 1
    fi

    # NB: Expects global var $swut
    local PATH="${TEST_PATH}:${PATH}"
    . $swut
}



exec_swut() {
    if [[ -z "$TEST_PATH" ]] ; then
        echo "# ${FUNCNAME[0]}: \$TEST_PATH unset!" >&3
        return 1
    fi

    # NB: Expects global var $swut
    local PATH="${TEST_PATH}:${PATH}"
# `bash` doesn't searcn $PATH ...?
#   bash $swut "$@"
    $swut "$@"
} ; export -f exec_swut



print_status() {
    echo "# status: '$status'" >&3
}



print_output() {
    local -ir qty_lines=${#lines[*]}

    echo "# lines ($qty_lines total):" >&3
    for i in $( seq 0 $((qty_lines-1)) ) ; do
        echo "#   $i: ${lines[$i]}" >&3
    done
}



print_results() {
    print_status
    print_output
}
