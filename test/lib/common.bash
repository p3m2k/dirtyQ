set -o nounset  # Disallow unset variables and parameters.
set -o noglob   # Disable pathname expansion
set -o pipefail # Pipelines return value is exit value of last failing command
