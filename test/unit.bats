load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash

load helpers



# Test Support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!
    # Global vars declared in sourced SWUT can be overridden in test cases with
    # `local VARNAME=VAL` declarations.

    assert_dependencies $dependencies
}


setup() {
    mock="mock -d $BATS_TEST_TMPDIR"
}



# Common Tests
#

@test "echo_verbose()" {
    local output="This is only a test."

    source_swut "$swut"
    local -r verbose=1
    run echo_verbose "$output"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$output" ]]  # FIXME: confirm stderr
}


@test "echo_func_err()" {
    local err_msg="This is only a test."

    source_swut "$swut"
    run echo_func_err "$err_msg"

    assert_status $expected_status_success
    assert_lines_qty 1
    # NB: The calling function name depends on bats internals!
    [[ "${lines[0]}" == "bats_merge_stdout_and_stderr: $err_msg" ]]  # FIXME: confirm stderr
}


@test "_parse_cli_opts()" {
    # Is this even necessary?  We'd mostly be testing `getopts`.
    skip
}



# Script-specific
#

@test "is_dirtyq(), non-existent path" {
    local ds_path="/path/to/nowhere"

    source_swut "$swut"
    local -r verbose=1
    run is_dirtyq "$ds_path"

    assert_status $expected_status_error
    assert_lines_qty 1
    [ "${lines[0]}" = "Specified path does not exist: '$ds_path'" ]
}


@test "is_dirtyq(), dirtyq file" {
    # Need a real file to pass the [[ tests.
    local ds_path="$(make_temp_ds)"

    source_swut "$swut"
    local -r verbose=1
    $mock f_get_header "$fake_magic_string"
    run is_dirtyq "$ds_path"

    assert_status 2
    assert_lines_qty 1
    [ "${lines[0]}" = "Specified path is a dirtyQ file: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_F_GET_HEADER_CALLS_DIR}/1.txt" "$ds_path"
}


@test "is_dirtyq(), dirtyq directory" {
    # Need a real dir and file to pass the [[ tests.
    local ds_path="$(make_temp_ds -d)"
    local magic_file_path="${ds_path}/README.txt"

    source_swut "$swut"
    local -r verbose=1
    $mock f_get_header "$fake_magic_string"
    run is_dirtyq "$ds_path"

    assert_status 3
    assert_lines_qty 1
    [ "${lines[0]}" = "Specified path is a dirtyQ directory: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_F_GET_HEADER_CALLS_DIR}/1.txt" "${ds_path}/README.txt"
}


@test "is_dirtyq(), unrecognized node" {
    # Need a real file to pass the [[ tests.
    local ds_path="$(make_temp_ds)"

    source_swut "$swut"
    local -r verbose=1
    $mock f_get_header "Clearly not a dirtyQ file!"
    run is_dirtyq "$ds_path"

    assert_status 4
    assert_lines_qty 1
    [ "${lines[0]}" = "Specified path is not a dirtyQ data structure: '$ds_path'" ]
    # Calls to f_get_header() are thoroughly tested in previous cases.
}


@test "f_get_header()" {
    local fake_queue_contents="$expected_magic_string
foo
bar"

    source_swut "$swut"
    run f_get_header <(echo "$fake_queue_contents")

    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_magic_string" ]
}


@test "f_get_data(), empty queue" {
    local fake_queue_contents="$fake_magic_string"

    source_swut "$swut"
    run f_get_data <(echo "$fake_queue_contents")

    assert_status $expected_status_empty
    assert_lines_qty 0
}


@test "f_get_data()" {
    local fake_queue_contents="$expected_magic_string
foo
bar"

    source_swut "$swut"
    run f_get_data <(echo "$fake_queue_contents")

    assert_lines_qty 2
    [ "${lines[0]}" = "foo" ]
    [ "${lines[1]}" = "bar" ]
}


@test "f_enq()" {
    local ds_path="$(make_temp_ds)"
    local new_data="My voice is my password."

    source_swut "$swut"
    run f_enq "$ds_path" "$new_data"

    local expected_queue_contents="$fake_magic_string
$new_data"
    assert_status 0
    assert_file_cmp_string "$ds_path" "$expected_queue_contents"
}


@test "f_deq(), empty queue" {
    source_swut "$swut"
    $mock f_get_data
    run f_deq "/does/not/matter"

    assert_status $expected_status_empty
    assert_lines_qty 0
}


@test "f_deq()" {
    local expected_data="I am a little teapot..."
    local other_data="hurrr"
    local ds_path="$(make_temp_ds "$expected_data" "$other_data")"

    source_swut "$swut"
    $mock f_get_header "$fake_magic_string"
    run f_deq "$ds_path"

    local expected_queue_contents="$fake_magic_string
$other_data"
    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_data" ]
    assert_file_cmp_string "$ds_path" "$expected_queue_contents"
}


@test "f_push()" {
    local other_data="I am haunted by waters."
    local ds_path="$(make_temp_ds "$other_data")"
    local new_data="My voice is my password."

    source_swut "$swut"
    run f_push "$ds_path" "$new_data"

    local expected_stack_contents="$fake_magic_string
$new_data
$other_data"
    assert_status 0
    assert_lines_qty 0
    assert_file_cmp_string "$ds_path" "$expected_stack_contents"
}


@test "f_pop()" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock f_deq
    run f_pop "$ds_path"

    assert_file_cmp_string "${MOCK_F_DEQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_all_files(), empty" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock ls
    run d_get_all_files "$ds_path"

    assert_status 0
    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_LS_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_first_file(), empty" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock d_get_all_files
    run d_get_first_file "$ds_path"

    assert_status 0
    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_last_file(), empty" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock d_get_all_files
    run d_get_last_file "$ds_path"

    assert_status 0
    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_all_files(), one object" {
    local ds_path="/does/not/matter"
    local fake_queue_file="42"

    source_swut "$swut"
    $mock ls "$fake_queue_file"
    run d_get_all_files "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_queue_file" ]
    assert_file_cmp_string "${MOCK_LS_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_first_file(), one object" {
    local ds_path="/does/not/matter"
    local fake_queue_file="1"

    source_swut "$swut"
    $mock d_get_all_files "$fake_queue_file"
    run d_get_first_file "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_queue_file" ]
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_last_file(), one object" {
    local ds_path="/does/not/matter"
    local fake_queue_file="1"

    source_swut "$swut"
    $mock d_get_all_files "$fake_queue_file"
    run d_get_last_file "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_queue_file" ]
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_all_files(), two objects" {
    local ds_path="/does/not/matter"
    local fake_queue_file_1="2"
    local fake_queue_file_2="3"

    source_swut "$swut"
    $mock ls "$fake_queue_file_1
$fake_queue_file_2"
    run d_get_all_files "$ds_path"

    assert_status 0
    assert_lines_qty 2
    [ "${lines[0]}" = "$fake_queue_file_1" ]
    [ "${lines[1]}" = "$fake_queue_file_2" ]
    assert_file_cmp_string "${MOCK_LS_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_first_file(), two objects" {
    local ds_path="/does/not/matter"
    local fake_queue_file_1="2"
    local fake_queue_file_2="3"

    source_swut "$swut"
    $mock d_get_all_files "$fake_queue_file_1
$fake_queue_file_2"
    run d_get_first_file "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_queue_file_1" ]
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_get_last_file(), two objects" {
    local ds_path="/does/not/matter"
    local fake_queue_file_1="2"
    local fake_queue_file_2="3"

    source_swut "$swut"
    $mock d_get_all_files "$fake_queue_file_1
$fake_queue_file_2"
    run d_get_last_file "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_queue_file_2" ]
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_enq()" {
    local ds_path="$(make_temp_ds -d)"
    local other_data_file="42"
    local new_data="My voice is my password."

    source_swut "$swut"
    $mock d_get_last_file "$other_data_file"
    run d_enq "$ds_path" "$new_data"

    assert_status 0
    assert_lines_qty 0
    assert_file_cmp_string "${ds_path}/43" "$new_data"
    assert_file_cmp_string "${MOCK_D_GET_LAST_FILE_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_deq(), empty queue" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock d_get_first_file
    run d_deq "$ds_path"

    assert_status $expected_status_empty
    assert_lines_qty 0
    assert_file_cmp_string "${MOCK_D_GET_FIRST_FILE_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_deq()" {
    local expected_data="I am a little teapot..."
    local ds_path="$(make_temp_ds -d "$expected_data")"

    source_swut "$swut"
    $mock d_get_first_file "1"
    run d_deq "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_data" ]
    assert_file_not_found "${ds_path}/1"
    assert_file_cmp_string "${MOCK_D_GET_FIRST_FILE_CALLS_DIR}/1.txt" "$ds_path"
}


@test "d_push(), empty queue" {
    local ds_path="$(make_temp_ds -d)"
    local new_data="My voice is my password."

    source_swut "$swut"
    $mock d_get_first_file
    run d_push "$ds_path" "$new_data"

    assert_status $expected_status_success
    assert_file_cmp_string "${MOCK_D_GET_FIRST_FILE_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${ds_path}/1" "$new_data"
}


@test "d_push(), room at the top" {
    local ds_path="$(make_temp_ds -d)"
    local new_data="My voice is my password."

    source_swut "$swut"
    $mock d_get_first_file "42"
    run d_push "$ds_path" "$new_data"

    assert_status $expected_status_success
    assert_file_cmp_string "${MOCK_D_GET_FIRST_FILE_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${ds_path}/41" "$new_data"
}


@test "d_push(), no room at the top" {
    local ds_path="$(make_temp_ds -d)"
    local new_data="My voice is my password."

    source_swut "$swut"
    $mock d_get_first_file "1"
    $mock d_get_all_files "1
2
3"
    $mock mv
    run d_push "$ds_path" "$new_data"

    assert_file_cmp_string "${MOCK_D_GET_FIRST_FILE_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_MV_CALLS_DIR}/1.txt" "${ds_path}/3 ${ds_path}/4"
    assert_file_cmp_string "${MOCK_MV_CALLS_DIR}/2.txt" "${ds_path}/2 ${ds_path}/3"
    assert_file_cmp_string "${MOCK_MV_CALLS_DIR}/3.txt" "${ds_path}/1 ${ds_path}/2"
    assert_file_cmp_string "${ds_path}/1" "$new_data"
    assert_status $expected_status_success
}


@test "d_pop()" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock d_deq
    run d_pop "$ds_path"

    assert_file_cmp_string "${MOCK_D_DEQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "initialize_dirtqy(), existing, dirtyQ structure" {
    local ds_path="$(make_temp_ds)"

    source_swut "$swut"
    local -r verbose=1
    run initialize_dirtyq "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 2
    [ "${lines[0]}" = "Specified path is a dirtyQ file: '$ds_path'" ]
    [ "${lines[1]}" = "There's already a dirtyQ data structure here: '$ds_path'" ]
}


@test "initialize_dirtqy(), existing, non-dirtyQ" {
    local ds_path="${BATS_TEST_TMPDIR}/someone_elses_file"
    touch "$ds_path"

    source_swut "$swut"
    local -r verbose=1
    run initialize_dirtyq "$ds_path"

    assert_status $expected_status_error
    assert_lines_qty 2
    [ "${lines[0]}" = "Specified path is not a dirtyQ data structure: '$ds_path'" ]
    [ "${lines[1]}" = "There's already something here: '$ds_path'" ]
}


@test "initialize_dirtqy(), file" {
    local ds_path="${BATS_TEST_TMPDIR}/temp_ds"

    source_swut "$swut"
    local -r verbose=1
    run initialize_dirtyq "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "Initializing data structure: '$ds_path'" ]
    assert_file_grep_string "$ds_path" "$expected_magic_string"
}


@test "initialize_dirtqy(), directory" {
    local ds_path="${BATS_TEST_TMPDIR}/temp_ds"

    source_swut "$swut"
    local -r verbose=1
    run initialize_dirtyq "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "Initializing data structure: '$ds_path'" ]
    assert_file_grep_string "$ds_path" "$expected_magic_string"
}


@test "length_of(), not a dirtyQ" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 4 is_dirtyq
    run length_of "$ds_path"

    assert_status 1
    assert_lines_qty 1
    [ "${lines[0]}" = "Not a dirtyQ data structure: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "length_of(), file, empty" {
    # This would be a worthwhile test case, but alas, due to the way
    # lib/mocker operates, it can't be done.  The _d_deq() call within
    # the mock f_get_data returns an empty string when called to dequeue
    # from an empty data structure.  Naturally, that includes a newline,
    # which length_of() counts as a data object.  Note that this does
    # not imply a flaw in dirtyq, itself.
    skip

    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 2 is_dirtyq
    $mock f_get_data
    local -ir expected_len=0
    run length_of "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_len" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_GET_DATA_CALLS_DIR}/1.txt" "$ds_path"
}


@test "length_of(), file, 1 object" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 2 is_dirtyq
    local -r fake_data=$(echo "foo")
    $mock f_get_data "$fake_data"
    local -ir expected_len=1
    run length_of "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_len" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_GET_DATA_CALLS_DIR}/1.txt" "$ds_path"
}


@test "length_of(), file, 3 objects" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 2 is_dirtyq
    local -r fake_data=$(echo "foo" ; echo "bar" ; echo "blat")
    $mock f_get_data "$fake_data"
    local -ir expected_len=3
    run length_of "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_len" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_GET_DATA_CALLS_DIR}/1.txt" "$ds_path"
}


@test "length_of(), directory, empty" {
    # NB: See comment on @test "length_of(), file, empty", above.
    skip

    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 3 is_dirtyq
    $mock d_get_all_files
    local -ir expected_len=0
    run length_of "$ds_path"

    assert_status $expected_len
    assert_lines_qty 1
    [ "${lines[0]}" = "Number of objects in data structure: '$expected_len'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_DIR}/1.txt" "$ds_path"
}


@test "length_of(), directory, 1 object" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 3 is_dirtyq
    local -r fake_data=$(echo "foo")
    $mock d_get_all_files "$fake_data"
    local -ir expected_len=1
    run length_of "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_len" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "length_of(), directory, 3 objects" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 3 is_dirtyq
    local -r fake_data=$(echo "foo" ; echo "bar" ; echo "blat")
    $mock d_get_all_files "$fake_data"
    local -ir expected_len=3
    run length_of "$ds_path"

    assert_status $expected_status_success
    assert_lines_qty 1
    [ "${lines[0]}" = "$expected_len" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_GET_ALL_FILES_CALLS_DIR}/1.txt" "$ds_path"
}


@test "enqueue(), not a dirtyQ" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    $mock -r 4 is_dirtyq
    run enqueue "$ds_path" "$new_data"

    assert_status 1
    assert_lines_qty 1
    [ "${lines[0]}" = "Not a dirtyQ data structure: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "enqueue(), file" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    local -r verbose=1
    $mock -r 2 is_dirtyq
    $mock f_enq
    run enqueue "$ds_path" "$new_data"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Enqueued data: '$new_data'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_ENQ_CALLS_DIR}/1.txt" "$ds_path $new_data"
}


@test "enqueue(), directory" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    local -r verbose=1
    $mock -r 3 is_dirtyq
    $mock d_enq
    run enqueue "$ds_path" "$new_data"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Enqueued data: '$new_data'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_ENQ_CALLS_DIR}/1.txt" "$ds_path $new_data"
}


@test "dequeue(), not a dirtyQ" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 4 is_dirtyq
    run dequeue "$ds_path"

    assert_status 1
    assert_lines_qty 1
    [ "${lines[0]}" = "Not a dirtyQ data structure: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "dequeue(), file" {
    local ds_path="/does/not/matter"
    local fake_data="blah blah"

    source_swut "$swut"
    $mock -r 2 is_dirtyq
    $mock f_deq "$fake_data"
    run dequeue "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_data" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_DEQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "dequeue(), directory" {
    local ds_path="/does/not/matter"
    local fake_data="blah blah"

    source_swut "$swut"
    $mock -r 3 is_dirtyq
    $mock d_deq "$fake_data"
    run dequeue "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_data" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_DEQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "push(), not a dirtyQ" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    $mock -r 4 is_dirtyq
    run push "$ds_path" "$new_data"

    assert_status 1
    assert_lines_qty 1
    [ "${lines[0]}" = "Not a dirtyQ data structure: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "push(), file" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    local -r verbose=1
    $mock -r 2 is_dirtyq
    $mock f_push
    run push "$ds_path" "$new_data"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Pushed data: '$new_data'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_PUSH_CALLS_DIR}/1.txt" "$ds_path $new_data"
}


@test "push(), directory" {
    local ds_path="/does/not/matter"
    local new_data="yadda yadda"

    source_swut "$swut"
    local -r verbose=1
    $mock -r 3 is_dirtyq
    $mock d_push
    run push "$ds_path" "$new_data"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "Pushed data: '$new_data'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_PUSH_CALLS_DIR}/1.txt" "$ds_path $new_data"
}


@test "pop(), not a dirtyQ" {
    local ds_path="/does/not/matter"

    source_swut "$swut"
    $mock -r 4 is_dirtyq
    run pop "$ds_path"

    assert_status 1
    assert_lines_qty 1
    [ "${lines[0]}" = "Not a dirtyQ data structure: '$ds_path'" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
}


@test "pop(), file" {
    local ds_path="/does/not/matter"
    local fake_data="blah blah"

    source_swut "$swut"
    $mock -r 2 is_dirtyq
    $mock f_pop "$fake_data"
    run pop "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_data" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_F_POP_CALLS_DIR}/1.txt" "$ds_path"
}


@test "pop(), directory" {
    local ds_path="/does/not/matter"
    local fake_data="blah blah"

    source_swut "$swut"
    $mock -r 3 is_dirtyq
    $mock d_pop "$fake_data"
    run pop "$ds_path"

    assert_status 0
    assert_lines_qty 1
    [ "${lines[0]}" = "$fake_data" ]
    assert_file_cmp_string "${MOCK_IS_DIRTYQ_CALLS_DIR}/1.txt" "$ds_path"
    assert_file_cmp_string "${MOCK_D_POP_CALLS_DIR}/1.txt" "$ds_path"
}





# vim: syntax=bash tw=0
