declare -gr fake_magic_string="## dirtyq v.0.1.0 :: 2022-05-25 12:00:00+00:00"



make_temp_ds() {
    local is_dir=0  # file by default
    local -r ds_path="${BATS_TEST_TMPDIR}/temp_ds"

    local opt OPTIND OPTARG OPTERR
    while getopts ":d" opt; do
        case $opt in
            d)
                is_dir=1
                ;;
            \?)
                exit $exit_error
                ;;
        esac
    done

    # Discard processed options.
    shift $((OPTIND-1))

    # Initialize
    if [[ $is_dir -eq 0 ]] ; then
        touch "$ds_path"
        echo "$fake_magic_string" > "${ds_path}"
        for data in "$@" ; do
            echo "$data" >> "$ds_path"
        done
    else
        mkdir "$ds_path"
        echo "$fake_magic_string" > "${ds_path}/README.txt"
        local -i i=1
        for data in "$@" ; do
            echo "$data" >> "${ds_path}/${i}"
            i=$((i+1))
        done
    fi

    echo "$ds_path"
}
