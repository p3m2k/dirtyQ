#!/usr/bin/env bash

# ${DESCRIPTION}
#
# Copyright (c) ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} (${COPYRIGHT_CONTACT})
# SPDX-License-Identifier: ${LICENSE_SPDX_ID}



# Important Stuff
#

set -o nounset  # Disallow unset variables and parameters.
set -o noglob   # Disable pathname expansion
set -o pipefail # Pipelines return value is exit value of last failing command





# Constants
#

readonly myname="$(basename "${BASH_SOURCE[0]}")"  # $BASH_SOURCE for compatability w/ bats testing.
readonly version="${RELEASE_VERSION}"

readonly usage_short="$myname [-h] [-v] [-V] [-d] init|length|enqueue|dequeue|push|pop PATH DATA"
readonly usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -d    Create directory-based data structure"

readonly exit_success=0
readonly exit_error=1
readonly exit_empty=2

readonly return_success=0
readonly return_error=1
readonly return_empty=2

readonly header_lines=1
readonly magic_string="## $myname v.${version} :: $(date --utc --rfc-3339=sec)"
readonly magic_file="README.txt"





# Defaults
#

# Make command line options global for sake of bats testing.
declare -gi verbose=0

declare -gi ds_dir_based=0





# Subroutines
#


echo_verbose() {
    # Echo output to stderr if verbose option is enabled.
    #
    # ARGS:
    local -r output="$@"
    #
    # OUTPUT: possible output to stderr
    #
    # RETURN VAL: implicit (echo)

    if (( verbose )) ; then
        echo "$output" >&2
    fi
}



echo_func_err() {
    # Echo function error message to stderr.
    #
    # ARGS: $@ passed on unmolested
    #
    # OUTPUT: output to stderr
    #
    # RETURN VAL: implicit (echo)

    echo "${FUNCNAME[1]}: $@" >&2
 }



is_dirtyq() {
    # Determines if a file or directory is a dirtyQ data structure.
    # IN: path to potential data structure
    local -r ds_path="$1"
    # OUT: string, user message, stderr, conditional
    # RETVAL:
    # - return_error = 1
    readonly return_is_dirtyq_file=2
    readonly return_is_dirtyq_dir=3
    readonly return_is_unrecognized=4

    readonly magic_test="## $myname v."

    if [[ ! -a "$ds_path" ]] ; then
        echo_verbose "Specified path does not exist: '$ds_path'"
        return $return_error

    elif [[ -f "$ds_path" ]] ; then
        f_get_header "$ds_path" | grep -q "$magic_test" >/dev/null
        if [[ $? -eq 0 ]] ; then
            echo_verbose "Specified path is a dirtyQ file: '$ds_path'"
            return $return_is_dirtyq_file
        fi

    elif [[ -d "$ds_path" ]]  &&  [[ -f "${ds_path}/${magic_file}" ]] ; then
        f_get_header "${ds_path}/${magic_file}" | grep -q "$magic_test" >/dev/null
        if [[ $? -eq 0 ]] ; then
            echo_verbose "Specified path is a dirtyQ directory: '$ds_path'"
            return $return_is_dirtyq_dir
        fi
    fi

    echo_verbose "Specified path is not a dirtyQ data structure: '$ds_path'"
    return $return_is_unrecognized
}



f_get_header() {
    # Gets header from a dirtyQ file.
    # IN:
    local -r file="$1"
    # OUT: string, newline-delimited header, stdout
    # RETVAL: implicit

    echo "$(head -${header_lines}  "$file")"
}



f_get_data() {
    # Gets data object(s) from a dirtyQ file.
    # IN:
    local -r file="$1"
    # OUT: string, newline-delimited data, stdout
    # RETVAL:
    # - return_empty = 2
    # - implicit

    local -r data="$(tail -n +$((header_lines+1)) "$file")"
    if [[ -z "$data" ]] ; then
        return $return_empty
    else
        echo "$data"
    fi
}



f_enq() {
    # Enqueues data to a dirtyQ file.
    # IN:
    local -r file="$1"
    local -r new_data="$2"
    # OUT: none
    # RETVAL: implicit

    echo "$new_data" >> "$file"
}



f_deq() {
    # Dequeues data from a dirtyQ file.
    # IN:
    local -r file="$1"
    # OUT: string, data line, stdout
    # RETVAL:
    # - return_empty = 2
    # - implicit

    local -r data="$(f_get_data "$file")"
    if [[ -z "$data" ]] ; then
        return $return_empty
    else
        echo "$data" | head -1
    fi

    local -r tmp_file=$(mktemp)
    f_get_header "$file" > "$tmp_file"
    echo "$data" | tail -n +2 >> "$tmp_file"
    mv -f "$tmp_file" "$file"
}



f_push() {
    # Pushes data to a dirtyQ file.
    # IN:
    local -r file="$1"
    local -r new_data="$2"
    # OUT: none
    # RETVAL: implicit

    local -r tmp_file=$(mktemp)
    f_get_header "$file" > "$tmp_file"
    echo "$new_data" >> "$tmp_file"
    f_get_data "$file" >> "$tmp_file"
    mv -f "$tmp_file" "$file"
}



f_pop() {
    # Pops data from a dirtyQ file.  Wrapper around f_deq().
    # IN:
    local -r file="$1"
    # OUT: see f_deq()
    # RETVAL: see f_deq()

    f_deq "$file"
}



d_get_all_files() {
    # Gets a list of all data file names in a dirtyQ directory.
    # IN:
    local -r dir="$1"
    # OUT: string, data file names sorted numerically, stdout
    # RETVAL: implicit

    echo "$(ls "$dir" | grep -v "$magic_file" | sort --numeric-sort)"
}



d_get_first_file() {
    # Gets the name of the first file (numerically least value) in a
    # dirtyQ directory.
    # IN:
    local -r dir="$1"
    # OUT: string, data file name, stdout
    # RETVAL: implicit

    echo "$(d_get_all_files "$dir"| head -1)"
}



d_get_last_file() {
    # Gets the name of the last file (numerically greatest value) in a
    # dirtyQ directory.
    # IN:
    local -r dir="$1"
    # OUT: string, data file name, stdout
    # RETVAL: implicit

    echo "$(d_get_all_files "$dir"| tail -1)"
}



d_enq() {
    # Enqueues data to a dirtyQ directory.
    # IN:
    local -r dir="$1"
    local -r new_data="$2"
    # OUT: none
    # RETVAL: implicit

    local -ir last_file="$(d_get_last_file "$dir")"
    local -ir next_file="$(("$last_file" + 1))"
    echo "$new_data" > "${dir}/${next_file}"
}



d_deq() {
    # Dequeues data from a dirtyQ directory.
    # IN:
    local -r dir="$1"
    # OUT: string, newline-delimited data, stdout
    # RETVAL:
    # - return_empty = 2
    # - implicit

    local -ir first_file="$(d_get_first_file "$dir")"
    if [[ "$first_file" -eq 0 ]] ; then
        return $return_empty
    else
        cat "${dir}/${first_file}"
        rm -f "${dir}/${first_file}"
    fi
}



d_push() {
    # Pushes data to a dirtyQ directory.
    # IN:
    local -r dir="$1"
    local -r new_data="$2"
    # OUT: none
    # RETVAL: implicit

    local -ir first_file="$(d_get_first_file "$dir")"
    local -i next_file

    if [[ "$first_file" -eq 0 ]] ; then
        next_file="1"
    elif [[ "$first_file" -ne 1 ]] ; then
        next_file="$((first_file-1))"
    else
        local -r files_rev_order="$(d_get_all_files "$dir" | sort --numeric-sort --reverse)"
        local -i file
        while read file ; do
            mv "${dir}/${file}" "${dir}/$((file+1))"
        done < <(echo "$files_rev_order")
        next_file="1"
    fi

    echo "$new_data" > "${dir}/${next_file}"
}



d_pop() {
    # Pops data from a dirtyQ directory.  Wrapper around d_deq().
    # IN:
    local -r file="$1"
    # OUT: see d_deq()
    # RETVAL: see d_deq()

    d_deq "$file"
}



initialize_dirtyq() {
    # Initializes a file or directory as a dirtyQ data structure.
    # IN:
    local -r ds_path="$1"
    # OUT:
    # - string, user message, stdout, conditional
    # - string, user message, stderr, conditional
    # RETVAL:
    # - return_error = 1
    # - implicit

    if [[ -a "$ds_path" ]] ; then
        is_dirtyq "$ds_path"
        if [[ $? -eq 2  ||  $? -eq 3 ]] ; then
            echo "There's already a dirtyQ data structure here: '$ds_path'"
            return $return_success
        else
            echo "There's already something here: '$ds_path'"
            return $return_error
        fi
    fi

    echo_verbose "Initializing data structure: '$ds_path'"
    if [[ $ds_dir_based -eq 1 ]] ; then
        mkdir -p "$ds_path"
        echo "$magic_string" > "${ds_path}/${magic_file}"
    else
        echo "$magic_string" > "${ds_path}"
    fi
}



length_of() {
    # Gets number of data objects in a dirtyQ data structure.
    # IN:
    local -r ds_path="$1"
    # OUT:
    # - integer
    # RETVAL:
    # - return_error = 1
    # - implicit

    is_dirtyq "$ds_path"
    local -ir ret=$?

    local -i len=0
    if [[ $ret -eq 2 ]] ; then
        len=$(f_get_data "$ds_path" | wc -l)
    elif [[ $ret -eq 3 ]] ; then
        len=$(d_get_all_files "$ds_path" | wc -l)
    else
        echo "Not a dirtyQ data structure: '$ds_path'" >&2
        return $return_error
    fi

    echo $len
}



enqueue() {
    # Enqueues data to a dirtyQ file or directory.  Validates that given
    # path points to a dirtyQ data structure first.
    # IN:
    local -r ds_path="$1"
    local -r new_data="$2"
    # OUT:
    # - string, user message, stdout, conditional
    # - string, user message, stderr, conditional
    # RETVAL:
    # - return_error = 1
    # - implicit

    is_dirtyq "$ds_path"
    local -ir ret=$?

    if [[ $ret -eq 2 ]] ; then
        f_enq "$ds_path" "$new_data"
    elif [[ $ret -eq 3 ]] ; then
        d_enq "$ds_path" "$new_data"
    else
        echo "Not a dirtyQ data structure: '$ds_path'" >&2
        return $return_error
    fi

    echo_verbose "Enqueued data: '$new_data'"
}



dequeue() {
    # Dequeues data from a dirtyQ file or directory.  Validates that
    # given path points to a dirtyQ data structure first.
    # IN:
    local -r ds_path="$1"
    # OUT:
    # - string, newline-delimited data, stdout
    # - string, user message, stdout, conditional
    # RETVAL:
    # - return_error = 1
    # - return_empty = 2 (implicit)
    # - implicit

    is_dirtyq "$ds_path"
    local -ir ret=$?

    if [[ $ret -eq 2 ]] ; then
        f_deq "$ds_path"
    elif [[ $ret -eq 3 ]] ; then
        d_deq "$ds_path"
    else
        echo "Not a dirtyQ data structure: '$ds_path'" >&2
        return $return_error
    fi
}



push() {
    # Pushes data to a dirtyQ file or directory.  Validates that given
    # path points to a dirtyQ data structure first.
    # IN:
    local -r ds_path="$1"
    local -r new_data="$2"
    # OUT:
    # - string, user message, stdout, conditional
    # - string, user message, stderr, conditional
    # RETVAL:
    # - return_error = 1
    # - implicit

    is_dirtyq "$ds_path"
    local -ir ret=$?

    if [[ $ret -eq 2 ]] ; then
        f_push "$ds_path" "$new_data"
    elif [[ $ret -eq 3 ]] ; then
        d_push "$ds_path" "$new_data"
    else
        echo "Not a dirtyQ data structure: '$ds_path'" >&2
        return $return_error
    fi

    echo_verbose "Pushed data: '$new_data'"
}



pop() {
    # Pops data from a dirtyQ file or directory.  Validates that given
    # path points to a dirtyQ data structure first.
    # IN:
    local -r ds_path="$1"
    # OUT:
    # - string, newline-delimited data, stdout
    # RETVAL:
    # - return_error = 1
    # - return_empty = 2 (implicit)
    # - implicit

    is_dirtyq "$ds_path"
    local -ir ret=$?

    if [[ $ret -eq 2 ]] ; then
        f_pop "$ds_path"
    elif [[ $ret -eq 3 ]] ; then
        d_pop "$ds_path"
    else
        echo "Not a dirtyQ data structure: '$ds_path'" >&2
        return $return_error
    fi
}



_parse_cli_opts() {
    # Process command line options.

    readonly cli_opts=":hvVd"
    local opt OPTIND OPTARG OPTERR

    while getopts "$cli_opts" opt; do
        case $opt in
            h)
                echo "$usage_long"  # Need quotes to preserve newlines!
                exit $exit_success
                ;;
            v)
                echo "$myname $version"
                exit $exit_success
                ;;
            V)
                verbose=1
                echo "Verbose mode requested." >&2
                ;;
            d)
                ds_dir_based=1
                ;;
            \?)
                echo "Invalid option: $OPTARG"
                echo "$usage_short" >&2
                exit $exit_error
                ;;
        esac
    done

    return $((OPTIND-1))  # Number of options to discard.
}



main() {
    _parse_cli_opts "$@"  ;  shift $?

    # Check for at least 2 args.
    if [[ $# -lt 2 ]]; then
        echo "$usage_short" >&2
        return $return_error
    fi

    declare -r action="$1"  &&  shift
    declare -r ds_path="$1"  &&  shift

    if [[ ${action::4} != "init" ]] ; then
        # Determine if data structure is file- or directory-based.
        # (Ignore what the user says.)
        if [[ -d "$ds_path" ]] ; then
            ds_dir_based=1
        else
            ds_dir_based=0
        fi
    fi

    # Find our input.
    if [[ -p /dev/stdin ]] ; then
        declare -r item="$(cat -)"
    else
        declare -r item="$@"
    fi


    # Do stuff.
    #

    local handler=''
    case $action in
        init*)
            initialize_dirtyq "$ds_path"
            ;;

        len*)
            length_of "$ds_path"
            ;;

        enq*)
            enqueue "$ds_path" "$item"
            ;;

        deq*)
            dequeue "$ds_path" "$item"
            ;;

        pu*)
            push "$ds_path" "$item"
            ;;

        po*)
            pop "$ds_path" "$item"
            ;;

        *)
            echo "Unrecognized action."
            echo "$usage_short"
            exit $exit_error
            ;;
    esac
}





if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
fi
