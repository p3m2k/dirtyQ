#######
${SLUG}
#######

${DESCRIPTION}
==============

:author: ${AUTHOR_NAME}
:contact: ${AUTHOR_CONTACT}
:copyright: © ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} <${COPYRIGHT_CONTACT}>
:manual_section: FIXME
:version: ${RELEASE_VERSION}
:date: ${RELEASE_DATE}



Synopsis
--------

${SLUG} [-h] [-v] [-V] [-d] init|length|enqueue|dequeue|push|pop PATH DATA



Description
-----------

``${SLUG}`` is a command line program that provides simple, file system
based queue and stack data structures.  Data structures can be
implemented as single files, in which each line is a data object, or
directories, in which each data object is stored in a separate file.



Options
-------

-h
    Print usage.
-v
    Print version.
-V
    Verbose mode
-d
    Create directory-based data structure



Sub-Commands
------------

init
    Create a new ${SLUG} data structure.  Include the ``-d`` option to
    create a directory-based structure.

length
    Get the length of a data structure (number of data objects).

enqueue
    Add data to the end of a queue.

dequeue
    Remove data from the front of a queue and print to stdout.

push
    Add data to the top of a stack.

pop
    Remove data from the top of a stack and print to stdou.



Exit Status
-----------

0
    Success
1
    Failure
2
    Empty data structure



Examples
--------

``${SLUG}`` can take input from stdin in addition to a command line
argument.

Enqueue/push data from a pipeline:

.. code:: shell

   echo "MY VALUABLE DATA" | ${SLUG} enq PATH/TO/DATA/STRUCTURE

Enqueue/push data from a redirect:

.. code:: bash

   ${SLUG} enq PATH/TO/DATA/STRUCTURE < PATH/TO/VALUABLE/DATA

.. code:: bash

   ${SLUG} enq PATH/TO/DATA/STRUCTURE < <(echo "MY VALUABLE DATA")


``${SLUG}`` can be paired with a while loop that dequeues/pops data until
the structure is empty.

.. code:: shell

   while d=$(${SLUG} pop "PATH/TO/DATA/STRUCTURE") ; do
       useful-command "$t"
   done



See Also
--------

`Qrun <https://gitlab.com/p3m2k/Qrun>`_, a command-line program that
runs a command on each item in a ${SLUG}-based data structure.
